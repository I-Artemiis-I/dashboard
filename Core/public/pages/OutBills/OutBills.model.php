<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

$factures = new services\Seed('Factures');
$user = new services\Seed('User');


//Pagination
$nombre_factures = count($factures->search_in_table('*', array('etat'=>null)));
$rest = $nombre_factures % 10;
$re = 10;

$page = 0;
$_SESSION['rest'] = count(services\Tools::search_with('*', 'Factures', " WHERE id_facture BETWEEN $page AND $re "));; //etat IS NOT NULL AND

if(isset($_GET['limit'])){
    $compteur = 0;
    $page += intval($_GET['limit']);

    if(($page - $_SESSION['rest']) <= 1){

        $page = 0;

    }else if($page >= ($nombre_factures - $rest)){

        $page = ($nombre_factures - $rest) + 1 + $_SESSION['rest'];
        $_SESSION['rest'] = count(services\Tools::search_with('*', 'Factures', " WHERE id_facture BETWEEN $page AND $re ")); //etat IS NOT NULL AND
        $page = $page + $_SESSION['rest'];
        $re = $rest;
    }
}


$re = $re + $page;
$re = ($re + $_SESSION['rest']);





$result_Facture = services\Tools::search_with_jointure('*', 'Factures','User', " WHERE Factures.etat IS NULL AND Factures.id_user = User.id_user"); // AND id_facture BETWEEN $page AND $re


//print_r($resultatFacture); die();

//Voir la facture

if (isset($_GET['factureview']))
{
  $data = array('lien' => $_GET['factureview']);
  $res_factures_client = $factures -> search_in_table('id_user',$data);
  $id_user = $res_factures_client[0]['id_user'];

  $data = array('id_user'=> $id_user);
  $res_user = $user->search_in_table("*",$data);

        $file = './Core/public/folders/factures/'.$res_user[0]['folder'].'/'.$_GET['factureview'];

        if (file_exists($file))
        {
            header('Content-Disposition: inline; filename="'.$file.'"');
            header('Content-Type: application/pdf');
            header('Content-Length: '.filesize($file));
            readfile($file);
            exit;
        }

}

//Passer la facture en payée
if(isset($_GET['Pay']))
{
  $val=1;
  $id=$_GET['Pay'];

  $data = array('etat'=>$val);
  $condition=array('id_facture'=>$id);
  $update=$factures->update_table($data,$condition);
  $res= $factures->search_in_table("*", array("etat"=>$val,'id_facture'=>$id) );


            $_SESSION['flash'] = "Facture payée";
            $_SESSION['icon'] = "success";


        exit(header('location: /OutBills'));
}
