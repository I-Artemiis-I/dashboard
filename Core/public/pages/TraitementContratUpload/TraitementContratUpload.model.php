<?php
// name of project Makeflo.
// Script create by Lakhdar + Mylène
// Contact: lakhdar-rouibah@live.fr + mylene.nicollet@free.fr
// Web : rouibah.fr + https://mylene-nicollet.000webhostapp.com/

// instancie les tables Contrat, User, Souscrire, Messages
$contrat = new services\Seed('Contrat');
$user = new services\Seed('User');
$souscrire = new services\Seed('Souscrire');
$message = new services\Seed('Messages');

//Récupère tout ce qui a été envoyé par POST lors du TraitementContratNotif
$postUserId = $_POST['id_user'];
$postUserNom = $_POST['nom'];
$postUserPrenom = $_POST['prenom'];
$postAbonnementId = $_POST['id_abonnement'];
$postAbonnementNom = $_POST['nom_abonnement'];
$postDateDemande = $_POST['dateDemande'];


$mail = services\Tools::search_with('mail', 'User', "WHERE id_user = ".$postUserId.";");
$nomAbonnement = services\Tools::search_with('nom', 'Abonnement', "WHERE id_abonnement = ".$postAbonnementId.";");

if($_SERVER['REQUEST_METHOD'] == 'POST' && $_FILES['file']['name']){

    $support =   array('sssssssss','pdf');
    $ext = services\Tools::get_extension($_FILES['file']['name']);

    $key = array_search($ext, $support);

     if($_FILES['file']['name'] === ""){

         $_SESSION['flash'] = "Vous devez choisir un fichier";
         $_SESSION['icon'] = "danger";

    }else if($key == ""){

        $_SESSION['flash'] = "Format de fichier non supporté";
        $_SESSION['icon'] = "danger";

    }else{

        // get project folder in File
        $res_folder = services\Tools::search_with("*", "User", " WHERE mail='".$mail[0]."'");

        // check in table User if folder exist
        if(!$res_folder[0]['folder']){

            $name_folder = "FU".strtotime(date('Y-m-d'))."U".$res_folder[0]['id_user'];
            $data = array("folder" => $name_folder);
            $condition = array("id_user" => $res_folder[0]['id_user']);
            $user->update_table ($data, $condition);
            mkdir("./Core/public/folders/contracts/".$name_folder, 0755);
            copy('ht/.htaccess', "./Core/public/folders/contracts/".$name_folder."/.htaccess");
            $target = "./Core/public/folders/contracts/".$name_folder."/".$_FILES['file']['name'];

        }else if ($res_folder[0]['folder']){

            $dir = is_dir("./Core/public/folders/contracts/".$res_folder[0]['folder']);
            if($dir === false){
                mkdir("./Core/public/folders/contracts/".$res_folder[0]['folder'], 0755);
                copy('ht/.htaccess', "./Core/public/folders/contracts/".$res_folder[0]['folder']."/.htaccess");
            }

            $target = "./Core/public/folders/contracts/".$res_folder[0]['folder']."/".$_FILES['file']['name'];

        }

        // check if file exist in folder
        if (file_exists($target)) {

            $_SESSION['flash'] = "Ce fichier existe déjà !";
            // set icon danger
            $_SESSION['icon'] = "danger";

        }else{

            // upload file
            $up = move_uploaded_file($_FILES['file']["tmp_name"], $target);


            if($up){
                //Ajout du contrat dans la table Contrat de la BDD
                $post = array("date_contrat"=>date('Y-m-d'), "date_renouvellement"=>date("Y-m-d", strtotime("+1 year")),"lien"=>$_FILES['file']['name'], "id_user"=>$postUserId);
                $contrat->insert_in_table($post);


                //Update de Souscrire dans la BDD
                $idContrat = services\Tools::search_with('id_contrat', 'Contrat', "WHERE id_user = ".$postUserId." order by id_contrat desc;");
                $data = array("id_contrat"=>$idContrat[0]['id_contrat'],"date_achat"=>date('Y-m-d'), "date_exp"=>date("Y-m-d", strtotime("+1 year")), "traitement"=>"1");
                $condition = array("id_user"=>$postUserId);
                $souscrire->update_table_where_id_abonnement($data, $condition, $postAbonnementId);

                //Envoi du mail
                $res_user = services\Tools::search_with('*', 'User', "WHERE id_user = ".$postUserId."");
                $res_abonnement = services\Tools::search_with('*', 'Abonnement', "WHERE id_abonnement = ".$postAbonnementId."");

                $to = $res_user[0]['mail'];
                $mail_sub = "MAKEFLO - Nouveau contrat disponible";
                $msg = "Bonjour ".$postUserNom." ".$postUserPrenom.", \nMessage de Makeflo.\nVotre nouveau contrat pour le service ".$res_abonnement[0]['nom']." est désormais disponible dans votre espace personnel.\nMerci encore pour votre achat.";
                $email = services\Tools::send_mail($to, $mail_sub, $msg);

                //Envoi du message sur l'espace perso du client (fonctionne uniquement si le client a précédemment un message d'envoyé à l'admin avant)
                $msg2 ="Bonjour ".$postUserNom." ".$postUserPrenom.", votre nouveau contrat pour le service ".$res_abonnement[0]['nom']." est désormais disponible. Merci encore pour votre achat.";
                $tabmsg2 = array("message"=> $msg2, "date_message"=> date('Y-m-d H:i:s'), "nature"=>"response", "id_user"=>$postUserId);
                $message->insert_in_table($tabmsg2);


                $_SESSION['flash'] = "Votre fichier a été chargé avec succès. Un e-mail et un message ont été envoyé au client.";
                // set icon success
                $_SESSION['icon'] = "success";


                exit(header('location: /TraitementContratNotif'));

            }else{

                $_SESSION['flash'] = "Erreur de chargement de fichier !";
                // set icon danger
                $_SESSION['icon'] = "danger";
            }
        }
    }

}

?>
<?php ;
