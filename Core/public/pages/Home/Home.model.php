<?php

//Si le client a demander à ce qu'on le rappelle dans "Assistance technique" de Nav
if(isset($_POST['bouton'])){
	$message = new services\Seed('Messages');
	$telUser = services\Tools::search_with ("tel", "User", "WHERE id_user=".$_SESSION['login']['id']."");

				// Message de confirmation
				$_SESSION['flash'] = "Votre demande a bien été prise en compte. Nous vous rappellerons dès que possible.";

				// Si OK encadré vert
				$_SESSION['icon'] = "success";

        //Envoi de message à l'admin
        $msg ="Message automatique. Merci de me rappeler au ".$telUser[0]['tel'].".";
        $tabmsg = array("message"=> $msg, "date_message"=> date('Y-m-d H:i:s'), "nature"=>"send", "id_user"=>$_SESSION['login']['id']);
        $message->insert_in_table($tabmsg);
}

//Recupère toutes les actualites dans la BDD
$actualite = services\Tools::search_with ("*", "Actualite", "order by id_actualite desc");

//phpinfo();
