<?php
// name of project Makeflo.
// Script create by Sasorishi.
// Contact: on_alban@yahoo.fr.
// Name : Sasorishi

      $lesProjets = services\Tools::search_with("nom", "Project", "where id_user = ".$_SESSION['login']['id']);

      if(isset($_POST['charger'])) 
      {
        $support =   array('sssssssss', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'png', 'PNG', 'mov', 'MOV', 'mp4', 'MP4', 'mpeg', 'MPEG','mpeg4', 'MPEG4', 'tiff', 'TIFF','pdf', 'PDF', 'raw', 'RAW', 'wav', 'WAV', 'ogg', 'OGG', 'mp3', 'MP3', 'aiff', 'AIFF', 'wma', 'WMA', 'aac', 'AAC', 'bwf', 'BWF', 'doc', 'DOC', 'docx', 'DOCX', 'xlsx', 'XLSX', 'xml', 'XML', 'pptx', 'PPTX', 'avi', 'AVI', 'zip', 'ZIP');
        $ext = services\Tools::get_extension($_FILES['file']['name']);

        $key = array_search($ext, $support);

        if($_FILES['file']['name'] === ""){

            $_SESSION['flash'] = "Vous devez choisir un fichier";
            $_SESSION['icon'] = "danger";

        }
        else if($key == "")
        {

            $_SESSION['flash'] = "Extension de fichier non supportée";
            $_SESSION['icon'] = "danger";

        }
        else 
        {
          // get project folder 
          $res_project = services\Tools::search_with("*", "Project", " WHERE nom='".$_POST['nom']."'");

          $target = "./Core/public/folders/projects/".$res_project[0]['folder']."/".$_FILES['file']['name'];

          // check if file existe in folder
          if (file_exists($target)) 
          {

              $_SESSION['flash'] = "Ce fichier existe déjà !";
              // set icon danger
              $_SESSION['icon'] = "danger";

          }
          else 
          {

              // upload file
              $up = move_uploaded_file($_FILES['file']["tmp_name"], $target);

              if($up)
              {

                  $_SESSION['flash'] = "Votre fichier a été chargé avec succès";
                  // set icon danger
                  $_SESSION['icon'] = "success";
              }
              else 
              {
                  $_SESSION['flash'] = "Erreur de chargement de fichier !";
                  // set icon danger
                  $_SESSION['icon'] = "danger";
              }
          }
        }
      }