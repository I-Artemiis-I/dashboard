<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr


// inbstancier la table User
$login = new services\Seed('User');
$token = new services\Seed('Token');
$pass = new services\Seed('Pass');

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $res_admin = $login->search_in_table('*', array('mail' => $_POST['mail']));
    // if is the first connexion do this 
    if($res_admin):

        $_SESSION['flash'] = "Ce compte existe déjà !";
        // set icon danger
        $_SESSION['icon'] = "danger";

    else :

        
        // check is empty
        $table = array('type','nom', 'prenom', 'tel', 'mail', 'password');
        $retour = services\Tools::is_empty($_POST, $table);

        if($retour === null):
            // insert data $post in table user and get result of insert in variable $ retuirn
            $return = $login->insert_in_table($_POST);
            // if $return 
            if($return):

                // set flash message "type of error"
                $_SESSION['flash'] = $return;
                // set icon danger
                $_SESSION['icon'] = "danger";


            // else 
            else:

                $data = [
                'email'     => $_POST['mail'],
                'status'    => 'subscribed',
                'firstname' => $_POST['prenom'],
                'lastname'  => $_POST['mail']
                ];

                $mail = services\Tools::syncMailchimp($data);


                // set flash register success
                $_SESSION['flash'] = "Enregistré avec succès";
    
                // set icon danger
                $_SESSION['icon'] = "success";
                exit(header('location: /Register'));
            endif;
            // end 
        else :

            $_SESSION['flash'] = $retour;
            // set icon danger
            $_SESSION['icon'] = "danger";

        endif;



    endif;

    



}