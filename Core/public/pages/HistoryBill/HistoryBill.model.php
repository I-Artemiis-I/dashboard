<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

$factures = new services\Seed('Factures');
$user = new services\Seed('User');


$nombre_factures = count($factures->search_in_table('*', null));
$rest = $nombre_factures % 10;
$re = 10;

$page =0;

if(isset($_GET['limit'])){

    $page += intval($_GET['limit']);

    if($page <= 1){

        $page = 0;

    }else if($page >= ($nombre_factures - $rest)){

        $page = ($nombre_factures - $rest)+1;
        $re = $rest;
    }
}

$re = $re + $page;

$resultatFacture = services\Tools::search_with('*', 'Factures', ""); // WHERE id_facture BETWEEN $page AND $re


//Supprimer la facture de la base et dans le dossier
if(isset($_GET['delete'])){

	$folderClient = services\Tools::search_with('folder', 'User', " join Factures on User.id_user = Factures.id_user where id_facture =". $_GET['delete']."");
    $nomFichier = services\Tools::search_with('lien', 'Factures', " where id_facture =". $_GET['delete']."");

    $cheminFichier ='./Core/public/folders/factures/'.$folderClient[0]['folder'].'/'.$nomFichier[0]['lien'];

    unlink ($cheminFichier);

    $factures->delete_in_table (array("id_facture" => $_GET['delete']));

//Affichage du succès
    $_SESSION['flash'] = 'La facture a été supprimé avec succès';
    $_SESSION['icon'] = "success";

    exit(header('location: /HistoryBill'));

}


//Voir la factures
if (isset($_GET['factureview']))
{
  $data = array('lien' => $_GET['factureview']);
  $res_factures_client = $factures -> search_in_table('id_user',$data);
  $id_user = $res_factures_client[0]['id_user'];

  $data = array('id_user'=> $id_user);
  $res_user = $user->search_in_table("*",$data);

        $file = './Core/public/folders/factures/'.$res_user[0]['folder'].'/'.$_GET['factureview'];

        if (file_exists($file))
        {
            header('Content-Disposition: inline; filename="'.$file.'"');
            header('Content-Type: application/pdf');
            header('Content-Length: '.filesize($file));
            readfile($file);
            exit;
        }
}
