<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr
// Script Edit by Maëva.
//Contact: maevaabaul@outlook.com

// instancier la table Abonnement
$project = new services\Seed('Project');
$d = date('Y-m-d');

//Affiche les projets dont la date est passée
$res_project = services\Tools::search_with("*", "Project", "WHERE deadline < '$d'");

//Supprimer un projet
if(isset($_GET['delete'])):

// search in table project id_project
$id_pro = services\Tools::search_with("*", "Project", "WHERE id_project='".$_GET['delete']."'");
$id = $id_pro[0]['id_appointment'];


   $project->delete_in_table (array("id_project" => $_GET['delete']));

    $_SESSION['flash'] = 'Le Projet a été supprimé avec succès';
    $_SESSION['icon'] = "success";

    exit(header('location: /HistoryProjet'));
  endif;


  //Telecharger le fichier
  if (isset($_GET['Download'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Download'];

      if (file_exists($file)){
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="'.basename($file).'"');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file));
          readfile($file);
          // exit;
      }
  }

  //Voir le fichier
  if (isset($_GET['Eye'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Eye'];

      if (file_exists($file)){
          header('Content-Disposition: inline; filename="'.$file.'"');
          header('Content-Type: application/pdf');
          header('Content-Length: '.filesize($file));
          readfile($file);
          // exit;
      }
  }
