<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr


// instancier la table Abonnement
$abonnement = new services\Seed('Abonnement');
if(isset($_GET['id'])){
    $data = array("id_abonnement"=>$_GET['id']);
    $table = $abonnement->search_in_table("*", $data);
}
if(isset($_POST['modifier'])){// rien dedans
      $table = array('nom', 'description', 'prix','img');
      $retour = services\Tools::is_empty($_POST, $table);
      //update in table User
      $data = array('nom'=>$_POST['nom'], 'description'=>$_POST['description'], 'prix'=>$_POST['prix']);
      $condition = array('id_abonnement'=>$_GET['id']);
      $return = $abonnement->update_table($data, $condition);
      // check the image size
      $size = array(346, 467);
      $check_size = services\Tools::check_img($_FILES['img'], $size);
      if($check_size){
            $_SESSION['flash'] = $check_size;
            $_SESSION['icon'] = "danger";
      }else{ // search all in table Abonnement
            $arr_img = $abonnement->search_in_table("*", null);
            // if result
            if($arr_img){
                // get the name of img in the las insert id
                $name = $arr_img[count($arr_img)-1]['img'];
                $name_n = $name + 1;
            }else{
                $name_n = 1;
            }
            unlink($image); //suppression de l'ancienne
            // set directory
            $dir = "./Core/public/ressources/img/assets/";
            // set name of image
            $name = $name_n.".png";
            // upload image
            $upload = services\Tools::upload_img($_FILES['img'], $dir, $name);
            // add array image => name image
            $new_array = array("img" => $name_n);
            $condition = array('id_abonnement'=>$_GET['id']);
            // insert in the data base $_POST
            //$abonnement->insert_in_table($post);
            $abonnement->update_table($new_array,$condition);
            // return success
            $_SESSION['flash'] = 'Votre produit est modifier avec succès';
            $_SESSION['icon'] = "success";
            // exit to return on the same page
            }
      exit(header('location: /ModifServiceViewA/?id='.$_GET['id']));
}
