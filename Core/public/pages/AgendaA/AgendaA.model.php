<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

//Instancie la table User
$user = new services\Seed('User');

$res_user= services\Tools::search_with("*", "User"," where id_user = ".$_SESSION['login']['id']);

//$lesProjets = services\Tools::search_with("nom", "Project", "where id_user = ".$_SESSION['login']['id']);



// instancier la table Appointment
$appoint = new services\Seed('Appointment');
// instantiate class Month
$month = new services\Month($_GET['month'] ?? null, $_GET['year'] ?? null );

$all_appoint = $appoint->search_in_table('*', null);

// get the first day in month
$start_day = $month->getStartingDay();
$start_day = $start_day->format('N') === "1" ? $start_day : $month->getStartingDay()->modify('last monday');

if(isset($_GET['appoint'])){
    $res_appoint = $appoint->search_in_table('*', array('date_appoint' => $_GET['appoint']));

    //Vérifie dans la BDD s'il y a déjà un RDV à cette date (1RDV/jour)
    //Il existe déjà un RDV à cette date
    if($res_appoint){
        // set flash message "type of error"
        $_SESSION['flash'] = "Rendez-vous pris !";
        // set icon danger
        $_SESSION['icon'] = "danger";

    //Il n'existe pas de RDV à cette date
    }else{
        //Si cette date correspond à un dimanche, un samedi, un jour passé ou la date d'aujourd'hui alors renvoi erreur
        if($_GET['day'] == "Dim" || $_GET['day'] == "Sam" || $_GET['before'] == "true" || $_GET['appoint'] == date('Y-m-d')){
        // set flash message "type of error"
        $_SESSION['flash'] = "Rendez-vous impossible à cette date.";
        // set icon danger
        $_SESSION['icon'] = "danger";

        } else {

            if(isset($_POST['hour'])){
                //supprime les variables get au cas où l'internaute aurait modifier l'url
                unset($_GET['appoint']);
                unset($_GET['day']);
                unset($_GET['before']);

                $post = array("id_user"=>$_SESSION['login']['id'], "date_appoint"=> $_POST['appoint'], "hour_appoint"=>$_POST['hour']);
                $appoint->insert_in_table($post);

                $visibility = "hidden";

                //set flash message "rdv confirmé"
                $_SESSION['flash'] = "Votre rendez-vous le ".$_POST['appoint']." à ".$_POST['hour']." a été enregistré";
                //set icon success
                $_SESSION['icon'] = "success";
                exit(header('location: /AgendaA'));

            } else {
                $visibility = "visible";
                $dateAppoint = $_GET['appoint'];

            }
        }
    }

}



function appoint($dt, $all) {
    $run = null;

    if($all){
        foreach ($all as $val){
            if($dt == $val['date_appoint']){

                $run = "ok";
            }
        }
    }
    return $run;
}
