<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr
// Script Edit by Maëva.
//Contact: maevaabaul@outlook.com


//Instancie la table User
$user = new services\Seed('User');

//Instancie la table Contrat
$contrat = new services\Seed('Contrat');

// search user information
$res = services\Tools::search_with_jointure("*", 'User', 'Contrat', "WHERE User.id_user = Contrat.id_user " );

//Voir le contrat

if (isset($_GET['contractview']))
{
  $data = array('lien' => $_GET['contractview']);
  $res_contracts = $contrat -> search_in_table('id_user',$data);
  $id_user = $res_contracts[0]['id_user'];

  $data = array('id_user'=> $id_user);
  $res_user = $user->search_in_table("*",$data);

        $file = './Core/public/folders/contracts/'.$res_user[0]['folder'].'/'.$_GET['contractview'];

        if (file_exists($file))
        {
            header('Content-Disposition: inline; filename="'.$file.'"');
            header('Content-Type: application/pdf');
            header('Content-Length: '.filesize($file));
            readfile($file);
            exit;
        }

}
