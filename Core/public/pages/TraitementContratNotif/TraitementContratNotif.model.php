<?php
// name of project Makeflo.
// Script create by Lakhdar + Mylène
// Contact: lakhdar-rouibah@live.fr + mylene.nicollet@free.fr
// Web : rouibah.fr + https://mylene-nicollet.000webhostapp.com/

$contrat = new services\Seed('Contrat');
$res_contratATraiter = services\Tools::search_with('*', 'Souscrire', "WHERE traitement = 0");

function get_user($id){

    $user = new services\Seed('User');
    $table = $user->search_in_table('*', array('id_user'=>$id));
    return $table;
}
