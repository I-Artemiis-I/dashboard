<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

//Instancie la table user
$user = new services\Seed('User');
// Instancier la table Projet
$project = new services\Seed('Project');

// search user information
$res_user = $user->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));

// get name of user folder
$folder = $res_user[0]['folder'];
$res = $project->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));


//Affiche les projets Actuels
$d = date('Y-m-d');
$date = strtotime("$d +1 day");
$date = date('Y-m-d', $date);

$res = services\Tools::search_with('*', 'Project', "WHERE deadline BETWEEN '$d' AND '$date'");

  //Telecharger le fichier
  if (isset($_GET['Download'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Download'];

      if (file_exists($file)){
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="'.basename($file).'"');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file));
          readfile($file);
          // exit;
      }
  }

  //Voir le fichier
  if (isset($_GET['Eye'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Eye'];

      if (file_exists($file)){
          header('Content-Disposition: inline; filename="'.$file.'"');
          header('Content-Type: application/pdf');
          header('Content-Length: '.filesize($file));
          readfile($file);
          // exit;
      }
  }
