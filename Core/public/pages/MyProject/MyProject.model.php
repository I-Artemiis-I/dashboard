<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr
//namespace MyProject;

// instancier la table User
$user = new services\Seed('User');
$project = new services\Seed('Project');

// search user information
$res_user = $user->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));

// get name of user folder
$folder = $res_user[0]['folder'];
$res_project = $project->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));


//Télécharger un zip du projet entier
if(isset($_GET['folder']) and !isset($_GET['fichierVisu']) and !isset($_GET['fichierDown'])){

    $read = scandir ('./Core/public/folders/projects/'.$_GET['folder']);
    $zipname = 'file.zip';
    $zip = new \ZipArchive;
    $zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE);
    for($i = 3; $i<count($read); $i++) {
        $zip->addFile('./Core/public/folders/projects/'.$_GET['folder'].'/'.$read[$i], 'Makeflo/'.$read[$i]);

    }
    if ($zip->close() === false) {
        echo "Erreur lors de la création du .zip"; die();
    };

    if (file_exists($zipname)) {

        ob_clean();
        ob_end_flush();

        header('Content-Type: application/zip');
        header("Content-disposition: attachment; filename=\"".basename($zipname)."\"");
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);

        echo $read;
        $files1 = scandir($read);
        $files2 = scandir($read, 1);

        print_r($files1);
        print_r($files2);

    } else {

        exit("Aucun fichier à télécharger .zip n'a été trouvé");
    }
}


//Telecharger le fichier
if (isset($_GET['fichierDown'])){
    $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['fichierDown'];

    if (file_exists($file)){
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        // exit;
    }
}

//Voir le fichier
if (isset($_GET['fichierVisu'])){
    $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['fichierVisu'];

    if (file_exists($file)){
        header('Content-Disposition: inline; filename="'.$file.'"');
        header('Content-Type: application/pdf');
        header('Content-Length: '.filesize($file));
        readfile($file);
        // exit;
    }
}
