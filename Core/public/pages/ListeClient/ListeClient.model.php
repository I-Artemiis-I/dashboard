<?php
// name of project Makeflo.
// Script create by Alban
//                  BUYUK Cem 12/02/2019
// Contact: on_alban@yahoo.fr
//          cembuyuk7@gmail.com / www.buyukcem.com


$cli = new services\Seed('User');
$data = array("type"=>"user", "etat"=>NULL);
$res_clients = $cli->search_in_table("*", $data);

//Pagination
$nombre_user = count($cli->search_in_table('*', array('etat'=>NULL)));
$rest = $nombre_user % 10;
$re = 10;
$page = 0;
$_SESSION['rest'] = count(services\Tools::search_with('*', 'User', " WHERE id_user BETWEEN $page AND $re ")); //etat IS NOT NULL AND

if(isset($_GET['limit'])){
    $page += intval($_GET['limit']);
    if(($page - $_SESSION['rest']) <= 1){
        $page = 0;
    }else if($page >= ($nombre_user - $rest)){
        $page = ($nombre_user - $rest) + 1 + $_SESSION['rest'];
        $_SESSION['rest'] = count(services\Tools::search_with('*', 'User', " WHERE id_user BETWEEN $page AND $re ")); //etat IS NOT NULL AND
        $page = $page + $_SESSION['rest'];
        $re = $rest;
    }
}
$re = $re + $page;
$re = ($re + $_SESSION['rest']);

//Mettre l'utilisateur en etat=1  donc  ne plus l'afficher
if(isset($_GET['delete'])/*isset($_GET['delete'])*/){
        $val=1;
        $id=$_GET['delete'];

        $data = array('etat'=>$val);
        $condition=array('id_user'=>$id);
        $update=$cli->update_table($data,$condition);

        $res= $cli->search_in_table("*", array("etat"=>$val,'id_user'=>$id) );
        if ($res) {
          //Affichage du succès
          $_SESSION['flash'] = "Le client a été supprimé avec succès";
          $_SESSION['icon'] = "success";
        }else{
           print("<div class=\"alert alert-info\" role=\"alert\">
                        erreur impossible de supprrimer ce client
                  </div>");
        }exit(header('location: /ListeClient'));
}
