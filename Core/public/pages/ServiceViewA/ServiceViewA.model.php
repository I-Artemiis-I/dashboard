<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

// instancier la table Abonnement
$abonnement = new services\Seed('Abonnement');

if(isset($_GET['id'])){

    $data = array("id_abonnement"=>$_GET['id']);
    $table = $abonnement->search_in_table("*", $data);
}

// if post
if(isset($_POST['modifier']))
{
        //check is not empty
        $table = array('nom', 'description', 'prix');
        $retour = services\Tools::is_empty($_POST, $table);

        if(isset($_GET['id']))
        {
            //update in table User
            $data = array('nom'=>$_POST['nom'], 'description'=>$_POST['description'], 'prix'=>$_POST['prix']);
            $condition = array('id_abonnement'=>$_GET['id']);
            $return = $abonnement->update_table($data, $condition);

            // set flash register success
            $_SESSION['flash'] = "Enregistré avec succès";
            // set icon success
            $_SESSION['icon'] = "success";

            exit(header('location: /ServiceViewA/?id='.$_GET['id']));
        }
        else
        {

            $_SESSION['flash'] = $retour;
            // set icon danger
            $_SESSION['icon'] = "danger";

            exit(header('location: /ServiceViewA/?id='.$_GET['id']));
        }

}
