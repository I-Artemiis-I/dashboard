<?php 
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

// inbstancier la table Abonnement
$abonnement = new services\Seed('Abonnement');
$souscrire = new services\Seed('Souscrire');
$message = new services\Seed('Messages');

$table = $abonnement->search_in_table("*", null);

if(isset($_GET['id'])){

    $data = array("id_abonnement"=>$_GET['id']);
    $table = $abonnement->search_in_table("*", $data);
}

if (isset($_POST['ajouter'])) 
{
    // if isset GET
    if(isset($_GET['id'])){

        $array= array("id_abonnement"=>$_GET['id'], "id_user"=>$_SESSION['login']['id']);
        $res_service = $souscrire->search_in_table("*", $array);

        if(isset($res_service)){
            if($res_service[0]['traitement'] == 1){
                $_SESSION['flash'] = "Le service existe déjà !";
            }else{
                $_SESSION['flash'] = "La demande de service a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.";
            }

            // set icon danger
            $_SESSION['icon'] = "danger";

        }else{

            //Envoi notification de demande de contrat à l'admin
            //Insert une souscription dans la BDD
            $post = array("id_abonnement"=>$_GET['id'],"id_user"=>$_SESSION['login']['id'], "date_achat"=>date('Y-m-d H:i:s'), "date_exp"=>date('Y-m-d', strtotime('+1 years')), "traitement"=>'0');
            $souscrire->insert_in_table($post);

            //Envoi de message à l'admin
            $msg ="Message automatique. La demande du service ".$table[0]['nom']." a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.";
            $tabmsg = array("message"=> $msg, "date_message"=> date('Y-m-d H:i:s'), "nature"=>"send", "id_user"=>$_SESSION['login']['id']);
            $message->insert_in_table($tabmsg);


            $_SESSION['flash'] = "Vous avez ajouté un nouveau service. Merci de compter parmi nos clients. Après traitement, votre contrat sera disponible dans votre espace.";
            // set icon danger
            $_SESSION['icon'] = "success";
        }
}
    

}
