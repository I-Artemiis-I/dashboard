<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr
// Script Edit by Maëva.
//Contact: maevaabaul@outlook.com


//Instancie la table user
$user = new services\Seed('User');
// Instancier la table Projet
$project = new services\Seed('Project');

// search user information
$res_user = $user->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));

// get name of user folder
$folder = $res_user[0]['folder'];
$res_project = $project->search_in_table ("*", array("id_user"=>$_SESSION['login']['id']));


//Affiche les projets Actuels
$d = date('Y-m-d');
$res_project = services\Tools::search_with("*", "Project", "WHERE deadline > '$d'");


//Supprimer un projet
if(isset($_GET['delete'])):

// search in table project id_project
$id_pro = services\Tools::search_with("*", "Project", "WHERE id_project='".$_GET['delete']."'");
$id = $id_pro[0]['id_appointment'];


   $project->delete_in_table (array("id_project" => $_GET['delete']));

    $_SESSION['flash'] = 'Le Projet a été supprimé avec succès';
    $_SESSION['icon'] = "success";

    exit(header('location: /ListeProject'));
  endif;


  //Telecharger le fichier
  if (isset($_GET['Download'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Download'];

      if (file_exists($file)){
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="'.basename($file).'"');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file));
          readfile($file);
          // exit;
      }
  }

  //Voir le fichier
  if (isset($_GET['Eye'])){
      $file = './Core/public/folders/projects/'.$_GET['folder'].'/'.$_GET['Eye'];

      if (file_exists($file)){
          header('Content-Disposition: inline; filename="'.$file.'"');
          header('Content-Type: application/pdf');
          header('Content-Length: '.filesize($file));
          readfile($file);
          // exit;
      }
  }

  //Passer le Projet dans l'historique

  if(isset($_GET['End']))
  {
    $d = date('Y-m-d');
    $date = strtotime("$d -1 year");
    $date = date('Y-m-d', $date);

    $val = $date;
    $id = $_GET ['End'];

    $data = array('deadline' => $val);
    $condition = array('id_project'=> $id);

    $update = $project -> update_table($data,$condition);
    $res = $project -> search_in_table("*", array("deadline"=>$val,'id_project'=>$id ) );

    // Pouvoir faire passer l'etat à 1 si la facture passe dans l'historique
              $_SESSION['flash'] = "Projet passé dans l'historique avec succès";
              $_SESSION['icon'] = "success";


          exit(header('location: /ListeProject'));
  }
