<?php
// name of project Makeflo.
// Script create by Lakhdar.
// Contact: lakhdar-rouibah@live.fr.
// Web : rouibah.fr

// inbstancier la table User

$res_rdv = services\Tools::search_with('*', 'Appointment', "order by date_appoint");
$appoint = new services\Seed('Appointment');



// instantiate class Month
$month = new services\Month($_GET['month'] ?? null, $_GET['year'] ?? null );
$all_appoint = $appoint->search_in_table('*', null);

// get the first day in month
$start_day = $month->getStartingDay();
$start_day = $start_day->format('N') === "1" ? $start_day : $month->getStartingDay()->modify('last monday');

if(isset($_GET['appoint'])){
    $res_appoint = $appoint->search_in_table('*', array('date_appoint' => $_GET['appoint']));


    //Si cette date correspond à un dimanche, un samedi, un jour passé ou la date d'aujourd'hui alors renvoi erreur
    if($_GET['day'] == "Dim" || $_GET['day'] == "Sam" || $_GET['before'] == "true" || $_GET['appoint'] == date('Y-m-d')){
    // set flash message "type of error"
    $_SESSION['flash'] = "Rendez-vous impossible à cette date.";
    // set icon danger
    $_SESSION['icon'] = "danger";

  } else {

      if(isset($_POST['hour'])){
          //supprime les variables get au cas où l'internaute aurait modifier l'url
          unset($_GET['appoint']);
          unset($_GET['day']);
          unset($_GET['before']);

          $post = array("id_user"=>$_SESSION['login']['id'], "date_appoint"=> $_POST['appoint'], "hour_appoint"=>$_POST['hour']);
          $appoint->insert_in_table($post);

          $visibility = "hidden";

          //set flash message "rdv confirmé"
          $_SESSION['flash'] = "Votre rendez-vous le ".$_POST['appoint']." à ".$_POST['hour']." a été enregistré";
          //set icon success
          $_SESSION['icon'] = "success";
          exit(header('location: /RdvA'));


      } else {
          $visibility = "visible";
          $dateAppoint = $_GET['appoint'];

    }
  }

}

//Supprimer un rendez-vous sauf le jour même
if(isset($_GET['delete'])):

// search in table Appointment id_appointment
$App_id = services\Tools::search_with("*", "Appointment", "WHERE id_appointment='".$_GET['delete']."'");
$id = $App_id[0]['id_appointment'];

$date = strtotime($App_id[0]['date_appoint']);
$today = strtotime(date('Y-m-d'));

    if($App_id and $date == $today):

        $_SESSION['flash'] = 'Vous ne pouvez pas supprimer ce Rendez-vous !';
        $_SESSION['icon'] = "danger";

      else:

   $appoint->delete_in_table (array("id_appointment" => $_GET['delete']));

    $_SESSION['flash'] = 'Le Rendez-Vous a été supprimé avec succès';
    $_SESSION['icon'] = "success";

    exit(header('location: /RdvA'));
  endif;
endif;


function get_user($id){

    $user = new services\Seed('User');
    $table = $user->search_in_table('*', array('id_user'=>$id));
    return $table;
}
