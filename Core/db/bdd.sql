-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 12 avr. 2019 à 08:38
-- Version du serveur :  5.7.23
-- Version de PHP :  7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `makeflo`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

DROP TABLE IF EXISTS `abonnement`;
CREATE TABLE IF NOT EXISTS `abonnement` (
  `id_abonnement` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(80) NOT NULL,
  `description` varchar(255) NOT NULL,
  `prix` decimal(15,2) NOT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `img` int(11) NOT NULL,
  PRIMARY KEY (`id_abonnement`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `abonnement`
--

INSERT INTO `abonnement` (`id_abonnement`, `nom`, `description`, `prix`, `type`, `img`) VALUES
(1, 'Social Media - Community Management', 'Nous gérons vos réseaux sociaux et développons une communauté ciblée pour votre entreprise ou marque. ', '300.00', 1, 15),
(3, 'Créations Graphiques Illimités - À volonté', 'Nous réalisons pour vous votre contenu graphique numérique ou à faire imprimer selon vos besoins et à volonté.', '126.00', NULL, 9),
(4, 'Pack Vidéo Communication et Marketing', 'Vous êtes autonome niveau réalisation de tournage ou vous possédez beaucoup de rushes (vidéos brutes) dont vous voulez confier à un professionnel pour le montage ?\r\n\r\nComprend\r\nUne stratégie de communication élaborée avec le client\r\n\r\nUne animation logo s', '150.00', NULL, 3),
(7, 'Création de site web', 'Obtenez votre site web', '89.00', NULL, 8),
(8, 'Pack Vidéo Communication et Marketing', 'Pack vidéo', '150.00', 1, 4),
(9, 'Maintenance et sécurité de site vitrine', 'Nous gérons la maintenance et la sécurité de votre site vitrine au quotidien. ', '44.40', NULL, 10),
(10, 'Marketing box newsletter', 'Impressionnez vos clients et vos partenaires avec des newsletter', '250.00', NULL, 11),
(11, 'Référencement local SEO', 'Soyez en première page sur les moteurs de recherche comme Google, Bing ou Yahoo', '15.00', NULL, 12),
(12, 'Référencement payant SEO - SEA', 'Soyez présent sur les moteurs de recherches et les bannières publicitaires web', '150.00', NULL, 13),
(13, 'Vidéo motion design', 'Impressionnez vos clients et vos partenaires avec des vidéos ', '290.00', NULL, 14);

-- --------------------------------------------------------

--
-- Structure de la table `actualite`
--

DROP TABLE IF EXISTS `actualite`;
CREATE TABLE IF NOT EXISTS `actualite` (
  `id_actualite` int(11) NOT NULL AUTO_INCREMENT,
  `lien` text COLLATE latin1_general_ci NOT NULL,
  `titreAlt` text COLLATE latin1_general_ci NOT NULL,
  `img` int(11) NOT NULL,
  PRIMARY KEY (`id_actualite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Déchargement des données de la table `actualite`
--

INSERT INTO `actualite` (`id_actualite`, `lien`, `titreAlt`, `img`) VALUES
(1, 'https://dashboard.makeflo.tv/ServiceView/?id=1', 'nouveaute_social_media', 1),
(2, 'https://dashboard.makeflo.tv/ServiceView/?id=4', 'nouveaute_communication_video', 2),
(3, 'https://www.ontrack.com/fr/blog/conseils-proteger-piratage-donnees-internet-reseaux-sociaux-gestes-a-adopter/', 'nouveaute_article', 3);

-- --------------------------------------------------------

--
-- Structure de la table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
CREATE TABLE IF NOT EXISTS `appointment` (
  `id_appointment` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date_appoint` date NOT NULL,
  `hour_appoint` text NOT NULL,
  PRIMARY KEY (`id_appointment`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appointment`
--

INSERT INTO `appointment` (`id_appointment`, `id_user`, `date_appoint`, `hour_appoint`) VALUES
(33, 30, '2018-12-19', '14h00'),
(34, 2, '2018-12-26', '16h00'),
(35, 2, '2018-12-27', '16h00'),
(36, 2, '2019-01-04', '15h00'),
(37, 30, '2018-12-24', '18h00'),
(38, 2, '2019-02-08', '16h00'),
(40, 36, '2019-02-25', '14h00'),
(41, 36, '2019-03-01', '14h00'),
(42, 36, '2019-03-07', '14h00');

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `id_contrat` int(11) NOT NULL AUTO_INCREMENT,
  `date_contrat` date NOT NULL,
  `date_renouvellement` date NOT NULL,
  `lien` varchar(40) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_contrat`),
  KEY `Contrat_User0_FK` (`id_user`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`id_contrat`, `date_contrat`, `date_renouvellement`, `lien`, `id_user`) VALUES
(26, '2018-12-18', '2019-12-18', 'exempleContrat3.pdf', 30),
(27, '2018-12-22', '2019-12-22', 'exempleContrat.pdf', 30),
(28, '2019-01-21', '2020-01-21', 'exempleContrat3.pdf', 2),
(29, '2019-02-25', '2020-02-25', 'Project Manager.pdf', 36);

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

DROP TABLE IF EXISTS `factures`;
CREATE TABLE IF NOT EXISTS `factures` (
  `id_facture` int(11) NOT NULL AUTO_INCREMENT,
  `date_facture` date NOT NULL,
  `lien` varchar(40) NOT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  `msg` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_facture`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `factures`
--

INSERT INTO `factures` (`id_facture`, `date_facture`, `lien`, `etat`, `msg`, `id_user`) VALUES
(3, '2019-04-03', '', 1, NULL, 34),
(4, '2019-04-04', '', 1, NULL, 37),
(6, '2019-04-01', '', NULL, NULL, 30),
(9, '2019-03-08', '', NULL, NULL, 37),
(14, '2019-05-02', '', NULL, NULL, 34),
(16, '2019-06-02', '', NULL, NULL, 36),
(17, '2019-03-09', '', 1, NULL, 37),
(21, '2019-02-25', 'Project Manager.pdf', NULL, NULL, 36),
(22, '2019-02-26', 'exempleContrat3 (2).pdf', 1, NULL, 36),
(25, '2019-05-02', '', 1, NULL, 36),
(46, '2019-03-02', '', NULL, NULL, 34),
(65, '2019-08-01', '', NULL, NULL, 36);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `date_message` datetime NOT NULL,
  `nature` varchar(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `Messages_User0_FK` (`id_user`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id_message`, `message`, `date_message`, `nature`, `id_user`) VALUES
(27, 'Bonjour,\r\n\r\nJe suis au travail.\r\n\r\nMerci.', '2018-12-12 12:13:03', 'send', 2),
(28, 'Je voudrais un service Social Media please', '2018-12-12 12:29:47', 'send', 30),
(29, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-12 12:32:43', 'response', 30),
(30, 'Cool ! Maintenant je voudrais le deuxi&egrave;me aussi !', '2018-12-12 12:39:48', 'send', 30),
(31, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Communication vid&eacute;o motion design est désormais disponible. Merci encore pour votre achat.', '2018-12-12 12:40:39', 'response', 30),
(32, 'Un troisième service stp, juste pour voir si je reçoit bien un mail', '2018-12-12 13:15:19', 'send', 30),
(33, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Cr&eacute;ations Graphiques Illimit&eacu est désormais disponible. Merci encore pour votre achat.', '2018-12-12 13:16:06', 'response', 30),
(34, 'Message automatique. La demande du service Marketing box newsletter a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-13 08:39:49', 'send', 30),
(35, 'Message automatique. La demande du service Pack Vidéo Communication et Marketing a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-13 13:01:06', 'send', 2),
(36, 'Message automatique. La demande du service Cr&eacute;ation de site web et maintenance a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-13 13:02:33', 'send', 2),
(37, 'Message automatique. La demande du service Maintenance de site a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-13 13:02:45', 'send', 2),
(38, 'Message automatique. La demande du service Vid&eacute;o motion design a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-13 13:03:51', 'send', 30),
(39, 'zefezf', '2018-12-14 12:18:15', 'response', 2),
(40, 'okpk', '2018-12-14 12:28:50', 'send', 2),
(41, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-15 15:20:15', 'send', 30),
(42, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-15 15:28:42', 'response', 30),
(43, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 11:51:24', 'send', 30),
(44, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-17 12:08:53', 'response', 30),
(45, 'Message automatique. La demande du service Communication vid&eacute;o motion design a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 12:14:06', 'send', 30),
(46, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Communication vid&eacute;o motion design est désormais disponible. Merci encore pour votre achat.', '2018-12-17 12:15:39', 'response', 30),
(47, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 12:19:56', 'send', 30),
(48, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-17 12:20:38', 'response', 30),
(49, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 17:21:15', 'send', 30),
(50, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-17 17:33:09', 'response', 30),
(51, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 17:42:34', 'send', 30),
(52, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-17 17:43:26', 'response', 30),
(53, 'Message automatique. La demande du service Communication vid&eacute;o motion design a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-17 17:47:00', 'send', 30),
(54, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Communication vid&eacute;o motion design est désormais disponible. Merci encore pour votre achat.', '2018-12-17 17:48:09', 'response', 30),
(55, 'Message automatique. Merci de me rappeler au 0669608260.', '2018-12-18 15:37:34', 'send', 30),
(56, 'Message automatique. Merci de me rappeler au 09 86 70 51 11.', '2018-12-18 19:52:22', 'send', 2),
(57, 'Message automatique. La demande du service Social Media a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2018-12-20 10:15:14', 'send', 30),
(58, 'bonjour', '2018-12-21 17:02:49', 'response', 2),
(59, 'Bonjour Nicollet Mylène, votre nouveau contrat pour le service Social Media est désormais disponible. Merci encore pour votre achat.', '2018-12-22 16:03:15', 'response', 30),
(60, 'Message automatique. La demande du service Création de site web a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2019-01-21 10:17:37', 'send', 2),
(61, 'Bonjour MATEKY Edmas, votre nouveau contrat pour le service Création de site web est désormais disponible. Merci encore pour votre achat.', '2019-01-21 10:19:17', 'response', 2),
(62, 'Message automatique. Merci de me rappeler au 0669608260.', '2019-01-22 10:02:52', 'send', 30),
(63, 'Message automatique. La demande du service Marketing box newsletter a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2019-02-01 18:37:33', 'send', 2),
(64, 'Message automatique. La demande du service Créations Graphiques Illimités - À volonté a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2019-02-01 18:38:54', 'send', 2),
(65, 'Message automatique. Merci de me rappeler au 09 86 70 51 11.', '2019-02-01 18:43:03', 'send', 2),
(66, 'Mes projets', '2019-02-22 17:22:07', 'response', 2),
(67, 'Message automatique. La demande du service Pack Vidéo Communication et Marketing a bien été prise en compte. Après traitement, votre contrat sera disponible dans votre espace.', '2019-02-27 10:59:42', 'send', 36),
(68, 'Bonjour ABAUL  Maëva , votre nouveau contrat pour le service Pack Vidéo Communication et Marketing est désormais disponible. Merci encore pour votre achat.', '2019-02-27 11:00:59', 'response', 36);

-- --------------------------------------------------------

--
-- Structure de la table `pass`
--

DROP TABLE IF EXISTS `pass`;
CREATE TABLE IF NOT EXISTS `pass` (
  `id_token` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_token`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pass`
--

INSERT INTO `pass` (`id_token`, `id_user`) VALUES
(1, 2),
(14, 11),
(15, 11),
(16, 11),
(17, 11),
(20, 11),
(21, 11),
(23, 11),
(26, 11),
(27, 11),
(28, 11),
(29, 11),
(37, 11),
(38, 11),
(39, 11),
(40, 11),
(41, 11),
(42, 11),
(44, 11),
(46, 11),
(49, 11),
(56, 11),
(57, 11),
(60, 11),
(62, 11),
(64, 11),
(65, 11),
(66, 11),
(67, 11),
(68, 11),
(70, 11),
(71, 11),
(72, 11),
(73, 11),
(74, 11),
(75, 11),
(76, 11),
(77, 11),
(78, 11),
(79, 11),
(80, 11),
(81, 11),
(82, 11),
(83, 11),
(84, 11),
(85, 11),
(86, 11),
(87, 11),
(88, 11),
(89, 11),
(90, 11),
(91, 11),
(92, 11),
(93, 11),
(94, 11),
(95, 11),
(96, 11),
(97, 11),
(98, 11),
(99, 11),
(50, 32),
(51, 33),
(52, 33),
(53, 33),
(54, 33),
(55, 33),
(58, 33),
(59, 33),
(61, 33),
(63, 33),
(69, 33);

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id_project` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deadline` date NOT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  `folder` varchar(40) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_project`),
  KEY `Project_User0_FK` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `project`
--

INSERT INTO `project` (`id_project`, `nom`, `description`, `deadline`, `etat`, `folder`, `id_user`) VALUES
(1, 'Mylene', 'Jenesaispas', '2019-03-25', NULL, 'FP1544064336U2', 2),
(9, 'Blackout', 'Top 1', '2019-03-06', NULL, 'FP1551268204U36', 36),
(10, 'Assouan', 'Test', '2019-03-04', NULL, 'FP1551269242U37', 37),
(13, 'petit test', 'sas', '2018-03-06', NULL, 'FP1551346681U36', 36),
(19, 'TESSSSSST', 'T', '2019-04-03', NULL, 'FP1551326976U36', 36),
(20, 'AJAX', 'Tadic', '2019-03-06', NULL, 'FP1551875253U36', 36);

-- --------------------------------------------------------

--
-- Structure de la table `souscrire`
--

DROP TABLE IF EXISTS `souscrire`;
CREATE TABLE IF NOT EXISTS `souscrire` (
  `id_contrat` int(11) DEFAULT NULL,
  `id_abonnement` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_achat` date NOT NULL,
  `date_exp` date NOT NULL,
  `traitement` tinyint(1) NOT NULL DEFAULT '0',
  KEY `Souscrire_User1_FK` (`id_user`),
  KEY `id_abonnement` (`id_abonnement`,`id_user`) USING BTREE,
  KEY `id_contrat_FK` (`id_contrat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `souscrire`
--

INSERT INTO `souscrire` (`id_contrat`, `id_abonnement`, `id_user`, `date_achat`, `date_exp`, `traitement`) VALUES
(26, 13, 30, '2018-12-18', '2019-12-18', 1),
(27, 1, 30, '2018-12-22', '2019-12-22', 1),
(28, 7, 2, '2019-01-21', '2020-01-21', 1),
(NULL, 10, 2, '2019-02-01', '2020-02-01', 0),
(NULL, 3, 2, '2019-02-01', '2020-02-01', 0),
(29, 1, 36, '2019-02-25', '2020-02-25', 1),
(29, 8, 36, '2019-02-27', '2020-02-27', 1);

-- --------------------------------------------------------

--
-- Structure de la table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(40) NOT NULL,
  `date_token` date NOT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `token`
--

INSERT INTO `token` (`id_token`, `token`, `date_token`) VALUES
(1, 'a0c6eef5d4cbfe7c54b4bbc0b2678b2bd99d8065', '2018-11-29'),
(2, '3a41f42823bb8ef31c3b44a1794ccd89cde83e25', '2018-12-03'),
(3, 'e699b4aa842597bc91a9dfa74f6756bd9c759aab', '2018-12-03'),
(4, '0260fc84cddbd0bdb3a62a631a58e5d5823a4125', '2018-12-03'),
(5, 'a44537c6d6afd2e97e9211fca349bf7a7beeb353', '2018-12-04'),
(6, '86d161aa14848bed11f3f7dd92189bb165fd0754', '2018-12-04'),
(7, '11333fc0fcfcb46971df6b5bafb3004a6d1c9e3b', '2018-12-04'),
(8, '79b66ec42c734671bb7f9933b99e731e5de15877', '2018-12-05'),
(9, '50846dae5f265df8171d918850152b7b052fe3da', '2018-12-07'),
(10, '276b4212a93ec45e1ec8c58cd9afa483ce4ac3aa', '2018-12-07'),
(11, '7a6b700054ba1a0a2a9d9f5883eba5322514fa29', '2018-12-07'),
(12, 'e638ff62d4cae4880ef5d5359c2f24ba6348e157', '2018-12-11'),
(13, '0f9a576af5a9ff1339e7570dfe422190ac4e692b', '2018-12-11'),
(14, '380a3da77ae55276a56883b0cf3e8599b7be6933', '2018-12-11'),
(15, 'cc123032bc7bf59daca3a9b784c13e470fbab116', '2018-12-11'),
(16, 'd6c6a84c36441f36a2659023bba4c12266453fae', '2018-12-11'),
(17, '0fb586c290041eb86fd07cc5bf5cef3013f95ab4', '2018-12-11'),
(18, 'af3fd0381225895a3409050391a472afce350e6e', '2018-12-11'),
(19, '241a37e1e80bccd09fb7035781b4695cd7094f69', '2018-12-11'),
(20, '88c5277e00a339fe142211f1a6b98b43f7209dfd', '2018-12-11'),
(21, '955f3477d7eb3cd7550a93241723125ef7519a9b', '2018-12-11'),
(22, '2e0fd2d7282e0a4e0e37fe9ddbef530e33603abd', '2018-12-11'),
(23, 'eef87ada3f19e04f3700399f7c4d913d9e857228', '2018-12-11'),
(24, '2cb0f9af30ec1b72fd9355cf92cb5ce348633e70', '2018-12-11'),
(25, 'ec39b21f124635d01572eea0a0db7ea79459db04', '2018-12-11'),
(26, 'b36ed811dd65224b64a1212e4f855d90dcd85ace', '2018-12-11'),
(27, '64c8a0d8a2e41d5616d78eb77ea4965271dc0fda', '2018-12-11'),
(28, '2d79ee18682c6048e19cc09e6914b2c27899e2d0', '2018-12-11'),
(29, '6dc8f7508af9cf9bdf245a6bb8b56955cbe0cfbb', '2018-12-11'),
(30, '9febf372a8cc627ff90b633e8461a7749e11563e', '2018-12-11'),
(31, '92163502df5f356a463e881d117574937995e48e', '2018-12-11'),
(32, '448d0af1066fb2b350331f269655f772651cacb0', '2018-12-11'),
(33, 'd0431d4ac07c0491df7069a9536f389ebf5c0da3', '2018-12-11'),
(34, '3fdfcff978c3bf8a790594efdb82b23a68cd25fa', '2018-12-11'),
(35, '1a7a0d96c8ce0a8b7fb80961cf417ab79a0e6281', '2018-12-11'),
(36, 'e9d0148be043c6eecab276e2c446c2cc7d19b377', '2018-12-11'),
(37, 'b6d781cc1b2f00e104e7374107f311aed19a58a5', '2018-12-12'),
(38, '591a3273d1137883f7cc0f831eae0207f80e66a1', '2018-12-12'),
(39, 'b099bea2f86b7757a15d58e6c39b6aa216ea6c9f', '2018-12-12'),
(40, '94fd74e657dc0a4ae86f6a0786babadd8c7ce044', '2018-12-12'),
(41, 'e41b829b39eb0d8c2f7b29223c7fd9d3af4d0489', '2018-12-12'),
(42, 'c31dbd4838c8ad44f0fcad78c607559f8f880e68', '2018-12-12'),
(43, 'af7bbc0f3b2e750cd25a3502ea2eda4e2f3190e1', '2018-12-12'),
(44, '7bcf260060812c5580ff1ccfc9d7def5959772a1', '2018-12-12'),
(45, '3c45946968ab5390b0c37e8c45200c5c3c663a1e', '2018-12-12'),
(46, '8f77ad25dd037ee40b5cd22afa1413498c4cdff1', '2018-12-12'),
(47, 'c796e6d052f6a0af65705a0801479e61db83f226', '2018-12-12'),
(48, '52f99a4a6ef607024526823d513f1b1fb18eac3f', '2018-12-12'),
(49, '111233c0cb3ac07bfca39cb4fe7654e2151fed52', '2018-12-19'),
(50, '3d5c80abf3247599a2d36eb543e704bdbf8c303c', '2018-12-19'),
(51, '04e75c9205a84bf16304f2da3c60573aa771fcf3', '2018-12-19'),
(52, 'ff703ff9e2a106dad285ec0b4a2c45c52d0e4026', '2018-12-19'),
(53, 'ef703ba18dabb9a22e242cb400847f7829667ac2', '2018-12-19'),
(54, '7aec85be33123ce252e4fd2ee47d2241fafb8466', '2018-12-19'),
(55, 'c82d70741c7622b3174a4c1a5b2abf4f83b1d8c5', '2018-12-19'),
(56, 'c241ae7c7e0d689bbb7ec397f5c4f9101ef073c6', '2018-12-19'),
(57, '5d718c3d6009d19ca7a35e97ad77c7f9b2e0e9f5', '2018-12-19'),
(58, 'cfb09b13a12fa955f72b87511649cc6537bc3412', '2018-12-19'),
(59, 'c9d73a9ab248d2c34fbee3de3e8262fce97524da', '2018-12-19'),
(60, '5830a51179fe6724a53d416935373e2cfb7f4167', '2018-12-19'),
(61, 'bf53e5249ebbbb485485ba0f2ebd6c9587c00fa7', '2018-12-19'),
(62, '171d513e8008c26556bbebb5d1ead9b31074c4ed', '2018-12-19'),
(63, 'd310feed911833f4ac21142b612cfe398b4bd1ce', '2018-12-19'),
(64, '54c18b64a7677aaa9fa5a8fee925e444cc317e9a', '2018-12-19'),
(65, '24fda83af80125a09dbc77640aa3d382276fc343', '2018-12-19'),
(66, '0e77a1e71da831f0bbf39fc7cb97dc39764ee0a2', '2018-12-19'),
(67, '80ad6c59d9a5f73125ca6605ac53c6d7fb9d37c4', '2018-12-19'),
(68, '50fa15357dbaa212622730e7e5759e4c03aced30', '2018-12-19'),
(69, '53acec207e3dd03f20de150c3c78952e7bf148f4', '2018-12-19'),
(70, 'df7bfc7c5f78e39f3d02dd9868362d3a15c778fc', '2018-12-19'),
(71, '8f6b23c9a963469c0da0676dcef5df679941d28f', '2018-12-19'),
(72, '267ac5d6889dcb90e776e2e557531a0ff9485bd5', '2018-12-19'),
(73, '2363bc47e1f807ec28e9b6ceb7f4f483b1db74c2', '2018-12-19'),
(74, 'e9c411eebc87ee8bde4a874c002f8a1087d7d571', '2018-12-19'),
(75, '7263fd1ac313fb9d329de6d81fd94d0a3a0b7e71', '2018-12-19'),
(76, 'ab86344866a989271e0943395974a0d50d363e99', '2018-12-19'),
(77, '53a6170979123d55c50435bc8d6b3336e65fa1c3', '2018-12-19'),
(78, '8f554b60e5170eb46094d9ef1670507455a5c5b0', '2018-12-19'),
(79, '9a872884ae97069221a79cbe5978303019fb4f85', '2018-12-19'),
(80, '91e50559a61adfaaa5788943baf0a28184116129', '2018-12-19'),
(81, 'e9efa1e9e9edf4e02066ef856f8d02c2ce1e6a6a', '2018-12-19'),
(82, '8de0e54a8e9862b27488ae65ed19d94173929833', '2018-12-19'),
(83, '8fecbf3f7acb48ddfb3482c22e96c3506a43ecd9', '2018-12-19'),
(84, '6f6d5009183ca710872ed48ed3385e48ce45cafb', '2018-12-19'),
(85, 'b6bed7915c1ddab14449595a016ee60e2d625b06', '2018-12-20'),
(86, '42b881f1ef00d9a1b1c3070f02d5d85d5f8e9693', '2018-12-20'),
(87, 'e87bf485c19a98338e02dd1ed3c04ee9c76d495c', '2018-12-20'),
(88, '2d90c23ebeff2065229b0cd847376588201639e1', '2018-12-20'),
(89, 'f1494221b8f9d0b816fa966ed9eb2b2a12712105', '2018-12-20'),
(90, '9c9e0c20ae2a4235189a9db6716fd23ff0a445b5', '2018-12-20'),
(91, '8aa49f9fdc7f84a52413502e897ae3afce99d74d', '2018-12-20'),
(92, 'bafffc922470954f55bf33ff6492bba880a89c21', '2018-12-20'),
(93, 'd5de9575040277eaf95de4b573626c2160c01a36', '2018-12-20'),
(94, 'c9442cc2732f30e3d95148fa2f2d5d2e25d1453c', '2018-12-20'),
(95, 'ea99505785863037d9fe456eb83cae08130f31d2', '2018-12-20'),
(96, 'dfff0b4f9ef70c04f40691a64a243547f1b79edd', '2018-12-20'),
(97, 'ee88a6f96ed6207ae262a5e5e9c6839fc4efd392', '2018-12-20'),
(98, 'b29e31c61c332f311b320177fbb3b90cc87d5a1a', '2018-12-20'),
(99, 'ace3901cdbd47036df44ca400d4179802ef59186', '2018-12-20');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `tel` varchar(40) NOT NULL,
  `mail` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `folder` varchar(40) DEFAULT NULL,
  `type` varchar(40) NOT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `nom`, `prenom`, `tel`, `mail`, `password`, `folder`, `type`, `etat`) VALUES
(1, 'Mateky', 'Edmas', '0661782154', 'edmasmateky@gmail.com', '55611f4fb5c5f1709d9620c1d42f7d39cad74c21', NULL, 'super admin', NULL),
(2, 'MATEKY', 'Edmas', '09 86 70 51 11', 'makefloagency@gmail.com', '0861a3a26520b3a8fae0146523f1af537f7ec3f8', 'FU1543964400U2', 'user', NULL),
(11, 'On', 'Gia Toan', '0659729022', 'sasorishi@gmail.com', 'afd4d5a811e7dd7d161fb5f4769ceb0c1e5b1c07', 'FU1545087600U11', 'user', 1),
(30, 'Nicollet', 'Mylène', '0669608260', 'mylene.nicollet@gmail.com', '55611f4fb5c5f1709d9620c1d42f7d39cad74c21', 'FU1545087600U30', 'user', NULL),
(32, 'Nicollet', 'Myl&egrave;ne', '0669608260', 'mylene.nicollet@free.fr', '36a31ff331e6bab6708189333988a4bfb7946164', NULL, 'user', NULL),
(33, 'On', 'Gia Toan', '0659729022', 'on_alban@yahoo.fr', 'e75e9653c1f8275113e32d1f1b8cb5f5ece5ee3d', 'FU1545087600U11', 'user', NULL),
(34, 'Louis Marie', 'Manuel', '+33688998800', 'lmauto972@gmail.com', '32936669bfb9ecae91b92cf30a8b358f8986c959', NULL, 'user', NULL),
(35, 'Onoo', '  Gia Toan  ', '0659729022', 'gotux@shayzam.net', '0a6807c0856b137fb44ce239587e4f34e011b005', NULL, 'user', 1),
(36, 'ABAUL', ' Maëva ', '0627880027', 'maevaabaul@outlook.com', '55611f4fb5c5f1709d9620c1d42f7d39cad74c21', 'FU1550617200U36', 'user', NULL),
(37, 'Nlonda', 'Assouan', '0616953279', 'assouan_nlonda@yahoo.fr', '55611f4fb5c5f1709d9620c1d42f7d39cad74c21', NULL, 'user', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `Appointment_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD CONSTRAINT `Contrat_User0_FK` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `factures`
--
ALTER TABLE `factures`
  ADD CONSTRAINT `Factures_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `Messages_User0_FK` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `pass`
--
ALTER TABLE `pass`
  ADD CONSTRAINT `Pass_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Pass_ibfk_2` FOREIGN KEY (`id_token`) REFERENCES `token` (`id_token`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `Project_User0_FK` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `souscrire`
--
ALTER TABLE `souscrire`
  ADD CONSTRAINT `Souscrire_Abonnement0_FK` FOREIGN KEY (`id_abonnement`) REFERENCES `abonnement` (`id_abonnement`),
  ADD CONSTRAINT `Souscrire_Contrat_FK` FOREIGN KEY (`id_contrat`) REFERENCES `contrat` (`id_contrat`),
  ADD CONSTRAINT `Souscrire_User1_FK` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
